<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Swapi base URL
    |--------------------------------------------------------------------------
    |
    | This is the base URL that is used by the SwapiService for making requests to the Star Wars api.
    |
    */

    'base_url' => env('SWAPI_BASE_URL', 'https://swapi.dev/api/'),

];