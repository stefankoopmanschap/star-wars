<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PeopleController;
use App\Http\Controllers\PlanetController;
use App\Http\Controllers\SpeciesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/people')->name('people.')->group(function () {
    Route::get('/{people}', [PeopleController::class, 'show'])->name('show');
});

Route::prefix('/planet')->name('planet.')->group(function () {
    Route::get('/{planet}', [PlanetController::class, 'show'])->name('show');
});

Route::prefix('/species')->name('species.')->group(function () {
    Route::get('/{species}', [SpeciesController::class, 'show'])->name('show');
});