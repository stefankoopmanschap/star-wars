<?php

namespace App\Console\Commands;

use App\Services\SwapiService;
use App\Models\Species;
use Illuminate\Support\Str;
use Illuminate\Console\Command;

class SyncSpecies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:species';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command synchronizes all the results from the Star Wars API species resource';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param SwapiService $swapiService
     * @return void
     */
    public function handle(SwapiService $swapiService): void
    {
        // Create new empty collection which will be holding all species from the Star Wars API
        $allSpecies = collect();

        // Get all the species from the Start Wars API
        $species = $swapiService->species();

        // Return if the result is empty
        if (empty($species['results'])) {
            return;
        }

        // Push the results from the (first) api request onto the collection
        $allSpecies->push($species['results']);

        // Divide the count (amount of species) by 10 (10 species by request) so we know how much time we must call the resource
        $amountOfRequest = ceil($species['count'] / 10);

        // Do the other requests if needed
        for ($page = 2; $page <= $amountOfRequest; $page++) {
            $species = $swapiService->species($page);
            $allSpecies->push($species['results']);
        }

        // Flatten the array
        $allSpecies = $allSpecies->flatten(1);

        // Loop over the species and insert them
        foreach ($allSpecies as $species) {

            // Set the planetId
            $planetId = Str::of(rtrim($species['homeworld'], '/'))->split('/\//')->last();

            $model = Species::create([
                'id' => \Str::of(rtrim($species['url'], '/'))->split('/\//')->last(),
                'name' => $species['name'],
                'classification' => $species['classification'],
                'designation' => $species['designation'],
                'average_height' => $species['average_height'],
                'average_lifespan' => $species['average_lifespan'],
                'eye_colors' => $species['eye_colors'],
                'hair_colors' => $species['hair_colors'],
                'skin_colors' => $species['skin_colors'],
                'language' => $species['language'],
                'planet_id' => !empty($planetId) ? $planetId : null,
            ]);
        }
    }
}
