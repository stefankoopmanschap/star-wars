<?php

namespace App\Console\Commands;

use App\Services\SwapiService;
use App\Models\People;
use Illuminate\Support\Str;
use Illuminate\Console\Command;

class SyncPeople extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:people';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command synchronizes all the results from the Star Wars API people resource';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param SwapiService $swapiService
     * @return void
     */
    public function handle(SwapiService $swapiService): void
    {
        // Create new empty collection which will be holding all people from the Star Wars API
        $allPeople = collect();

        // Get all the people from the Start Wars API
        $people = $swapiService->people();

        // Return if the result is empty
        if (empty($people['results'])) {
            return;
        }

        // Push the results from the (first) api request onto the collection
        $allPeople->push($people['results']);

        // Divide the count (amount of persons) by 10 (10 persons by request) so we know how much time we must call the resource
        $amountOfRequest = ceil($people['count'] / 10);

        // Do the other requests if needed
        for ($page = 2; $page <= $amountOfRequest; $page++) {
            $people = $swapiService->people($page);
            $allPeople->push($people['results']);
        }

        // Flatten the array
        $allPeople = $allPeople->flatten(1);

        // Loop over the people and insert them
        foreach ($allPeople as $people) {
            $model = People::create([
               'id' => \Str::of(rtrim($people['url'], '/'))->split('/\//')->last(),
               'name' => $people['name'],
               'birth_year' => $people['birth_year'],
               'eye_color' => $people['eye_color'],
               'gender' => $people['gender'],
               'hair_color' => $people['hair_color'],
               'height' => $people['height'],
               'mass' => $people['mass'],
               'skin_color' => $people['skin_color'],
               'planet_id' => Str::of(rtrim($people['homeworld'], '/'))->split('/\//')->last(),
            ]);

            // Check if the people has any species and insert the relation into the database
            if (!empty($people['species'])) {
                $allSpecies = collect($people['species'])->map(function ($species) {
                    return \Str::of(rtrim($species, '/'))->split('/\//')->last();
                });

                $model->species()->attach($allSpecies->all());
            }
        }
    }
}
