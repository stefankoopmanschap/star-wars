<?php

namespace App\Console\Commands;

use App\Services\SwapiService;
use App\Models\Planet;
use Illuminate\Support\Str;
use Illuminate\Console\Command;

class SyncPlanets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:planets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command synchronizes all the results from the Star Wars API planets resource';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param SwapiService $swapiService
     * @return void
     */
    public function handle(SwapiService $swapiService): void
    {
        // Create new empty collection which will be holding all planets from the Star Wars API
        $allPlanets = collect();

        // Get all the planets from the Start Wars API
        $planets = $swapiService->planets();

        // Return if the result is empty
        if (empty($planets['results'])) {
            return;
        }

        // Push the planets from the (first) api request onto the collection
        $allPlanets->push($planets['results']);

        // Divide the count (amount of planets) by 10 (10 planets by request) so we know how much time we must call the resource
        $amountOfRequest = ceil($planets['count'] / 10);

        // Do the other requests if needed
        for ($page = 2; $page <= $amountOfRequest; $page++) {
            $planets = $swapiService->planets($page);
            $allPlanets->push($planets['results']);
        }

        // Flatten the array
        $allPlanets = $allPlanets->flatten(1);

        // Loop over the planets and insert them
        foreach ($allPlanets as $planet) {
            $model = Planet::create([
                'id' => \Str::of(rtrim($planet['url'], '/'))->split('/\//')->last(),
                'name' => $planet['name'],
                'diameter' => $planet['diameter'],
                'rotation_period' => $planet['rotation_period'],
                'orbital_period' => $planet['orbital_period'],
                'gravity' => $planet['gravity'],
                'population' => $planet['population'],
                'climate' => $planet['climate'],
                'terrain' => $planet['terrain'],
                'surface_water' => $planet['surface_water'],
            ]);
        }
    }
}
