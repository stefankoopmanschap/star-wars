<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Psy\Exception\RuntimeException;

class SwapiService
{

    /**
     * @var \Illuminate\Http\Client\PendingRequest
     */
    protected $client;

    /**
     * @var \Illuminate\Http\Client\Response
     */
    protected $requestResponse;

    /**
     * SwapiService constructor.
     */
    public function __construct()
    {
        $this->client = Http::withOptions([
            'base_uri' => config('swapi.base_url')
        ]);
    }

    /**
     * @param int $page
     * @return array
     * @throws RuntimeException
     */
    public function people(int $page = 1): array
    {
        $this->getRequest('people', $page);

        return $this->requestResponse;
    }

    /**
     * @param int $page
     * @return array
     * @throws RuntimeException
     */
    public function planets(int $page = 1): array
    {
        $this->getRequest('planets', $page);

        return $this->requestResponse;
    }

    /**
     * @param int $page
     * @return array
     * @throws RuntimeException
     */
    public function species(int $page = 1): array
    {
        $this->getRequest('species', $page);

        return $this->requestResponse;
    }

    /**
     * @param string $resource
     * @param int $page
     * @return void
     * @throws RuntimeException
     */
    private function getRequest(string $resource, int $page): void
    {
        try {
            // Do the request for the given resource
            $this->requestResponse = $this->client->get($resource, [
                'page' => $page,
            ]);

            // Throw a RunTimeException if the response is not OK
            if (!$this->requestResponse->ok()) {
                throw new \RuntimeException($this->requestResponse->getReasonPhrase());
            }

            // Set the body of the response
            $this->requestResponse = $this->requestResponse->json();

        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        }
    }


}