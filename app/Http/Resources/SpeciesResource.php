<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SpeciesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'classification' => $this->classification,
            'designation' => $this->designation,
            'average_height' => $this->average_height,
            'average_lifespan' => $this->average_lifespan,
            'eye_colors' => $this->eye_colors,
            'hair_colors' => $this->hair_colors,
            'skin_colors' => $this->skin_colors,
            'language' => $this->language,
        ];
    }
}