<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PeopleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'birth_year' => $this->birth_year,
            'eye_color' => $this->eye_color,
            'gender' => $this->gender,
            'hair_color' => $this->hair_color,
            'height' => $this->height,
            'height' => $this->height,
            'mass' => $this->mass,
            'skin_color' => $this->skin_color,
            'species' => SpeciesResource::collection($this->species),
            'planet' => new PlanetResource($this->planet),
        ];
    }
}
