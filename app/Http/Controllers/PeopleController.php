<?php

namespace App\Http\Controllers;

use App\Http\Resources\PeopleResource;
use App\Models\People;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * @param People $people
     * @return PeopleResource
     */
    public function show(People $people)
    {
        return new PeopleResource($people);
    }
}
