<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'birth_year',
        'eye_color',
        'gender',
        'hair_color',
        'height',
        'mass',
        'skin_color',
        'planet_id',
    ];

    /**
     * The species that belong to the people.
     */
    public function species()
    {
        return $this->belongsToMany(Species::class);
    }

    /**
     * Get the planet for the people.
     */
    public function planet()
    {
        return $this->belongsTo(Planet::class);
    }
}
