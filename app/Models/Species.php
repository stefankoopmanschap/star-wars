<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Species extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'classification',
        'designation',
        'average_height',
        'average_lifespan',
        'eye_colors',
        'hair_colors',
        'skin_colors',
        'language',
        'planet_id',
    ];

    /**
     * The people that belong to the species.
     */
    public function people()
    {
        return $this->belongsToMany(People::class);
    }

    /**
     * Get the planet for the species.
     */
    public function planet()
    {
        return $this->belongsTo(Planet::class);
    }
}
