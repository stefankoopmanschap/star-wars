<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Planet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'diameter',
        'rotation_period',
        'orbital_period',
        'gravity',
        'population',
        'climate',
        'terrain',
        'surface_water',
    ];

    /**
     * Get the people for the planet.
     */
    public function people()
    {
        return $this->hasMany(People::class);
    }

    /**
     * Get the species for the planet.
     */
    public function species()
    {
        return $this->hasMany(Species::class);
    }
}
